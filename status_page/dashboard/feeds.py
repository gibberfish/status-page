from django.contrib.syndication.views import Feed
from django.urls import reverse
from .models import Status

class LatestEntriesFeed(Feed):
    title = "Gibberfish Service Status Updates"
    link = "/"
    description = "Updates regarding service availability."

    def items(self):
        return Status.objects.order_by('-updated')[:10]

    def item_title(self, item):
        return item.updated.strftime('%B %d, %Y %H:%M')

    def item_description(self, item):
        return item.message
