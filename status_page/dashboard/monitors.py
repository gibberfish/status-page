import json, requests, time
from django.conf import settings
from datetime import datetime, timedelta, timezone
from .models import Monitor as MonitorModel

class MonitorList(object):
    """
    Maintains a list of Monitor objects
    """
    def __init__(self):
        self.api_key = settings.UPTIMEROBOT_API_KEY
        self.monitor_list = settings.UPTIMEROBOT_MONITOR_LIST
        self.api_url = settings.UPTIMEROBOT_API_URL
        self.monitors = []
        self.updated = None

    def clean_db(self):
        """
        Prunes old results from the db to prevent them from piling up
        """
        expired = datetime.now(timezone.utc) - timedelta(seconds=settings.MONITOR_CACHE_PRUNE_SECONDS)
        try:
            mm = MonitorModel.objects.filter(updated__lte=expired)
            mm.delete()
        except MonitorModel.DoesNotExist:
            pass

    def query_db(self):
        """
        Check the database for a cached copy of the monitor status
        """
        self.clean_db()
        try:
            mm = MonitorModel.objects.latest('updated')
            # if the update time + ttl is in the past, go fish
            if (int(mm.updated.strftime('%s')) + mm.ttl) < time.time():
                return None
            else:
                return mm
        except MonitorModel.DoesNotExist:
            # if there is no cached copy, go fish
            return None

    def query_api(self):
        """
        Queries the UptimeRobot API for the monitor status
        """
        r = requests.post(
            '%s/getMonitors?format=json' % self.api_url,
            data = {
                'api_key'   : self.api_key,
                'monitors'  : '-'.join(self.monitor_list),
                'custom_uptime_ratios': '7-30-180'
            },
            headers = {'cache-control': 'no-cache'}
        )
        r.raise_for_status()
        return json.dumps(r.json())

    def get_monitors(self):
        """
        First checks the cache, then fails back to the API to get the current
        monitor status. Caches new API queries.
        """
        mm = self.query_db()
        if mm is not None:
            self.updated = mm.updated.strftime('%B %d, %Y %H:%M')
            data = mm.data
        else:
            data = self.query_api()
            self.updated = time.strftime('%B %d, %Y %H:%M', time.gmtime())
            model = MonitorModel(data=data)
            model.save()
        monitors = json.loads(data)
        for m in monitors['monitors']:
            url = m['url']
            if m['port']:
                url = url + ':%s' % m['port']
            uptime = m['custom_uptime_ratio'].split('-')
            self.monitors.append(
                Monitor(
                    name    = m['friendly_name'],
                    status  = m['status'],
                    url     = url,
                    uptime = [
                        { 'days' : '7', 'percentage': uptime[0] },
                        { 'days' : '30', 'percentage': uptime[1] },
                        { 'days' : '180', 'percentage': uptime[2] },
                    ]
                )
            )
        return self.monitors

class Monitor(object):
    """
    Captures all the data we care about for each monitor
    """
    def __init__(self, name=None, status=None, url=None, port=None, uptime={}):
        self.name = name
        self.status = int(status)
        self.url = url
        self.port = port
        self.uptime = uptime
        if self.status == 2:
            self.ok = True
        else:
            self.ok = False

    def get(monitor):
        return {
            'name'  : self.name,
            'status': self.status,
            'url'   : self.url,
            'ok'    : self.ok,
            'uptime': self.uptime,
        }
