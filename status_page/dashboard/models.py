from django.db import models
from django.conf import settings

class Monitor(models.Model):
    updated = models.DateTimeField(auto_now=True)
    data = models.TextField()
    ttl = models.PositiveIntegerField(default=settings.MONITOR_CACHE_TTL_SECONDS)

class Status(models.Model):
    updated = models.DateTimeField(auto_now=True)
    message = models.TextField()
    label = models.TextField(max_length=128, default="")
    display = models.BooleanField(default=True)

    def get_absolute_url(self):
        return "/"
