from django.contrib import admin
from .models import Status

@admin.register(Status)
class StatusInline(admin.ModelAdmin):
    class Media:
        css = {
            "all": ("admin/css/custom.css",)
        }
    model = Status
    can_delete = True
    verbose_name_plural = 'statuses'
    ordering = ('updated',)
    list_display = ('updated', 'label', 'message')
