from django.shortcuts import render

from django.views.generic import TemplateView
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, redirect
from .models import Status
from .monitors import MonitorList

class DashboardView(TemplateView):
    template_name = 'dashboard/index.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        try:
            # look for manually added status updates to display
            status_list = Status.objects.filter(display=True).order_by('-updated')
        except Status.DoesNotExist:
            pass
        ml = MonitorList()
        context['monitors'] = ml.get_monitors()
        context['last_updated'] = ml.updated
        context['status_list'] = status_list
        return context
